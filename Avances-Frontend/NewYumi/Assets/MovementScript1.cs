﻿using UnityEngine;
using System.Collections;

public class MovementScript1 : MonoBehaviour {

	public float XForce;
	public float YForce;
	public float direction;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//direction = new Vector2 (0, -1);//Input.acceleration.y * XForce;
		rigidbody2D.AddForce (new Vector2(0,-1));
		if (Input.touchCount == 1) {
			rigidbody2D.AddForce(new Vector2(0, YForce));		
		}
	}
}
