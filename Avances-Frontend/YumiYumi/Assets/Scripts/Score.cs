﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public GUIText scoreText;
	public int ballValue;

	private int score;

	// Use this for initialization
	void Start () {
		score = 0;
		UpdateScore ();
	}
	void OnTriggerEnter2D(){
		score += ballValue;
		UpdateScore ();
	}
	
	// Update is called once per frame
	void UpdateScore () {
		scoreText.text = "Score:\n" + score;
	}
}
